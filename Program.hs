{-
Copyright 2013 Boyd Stephen Smith Jr.
Copyright 2013 Zac Slade

Licensing information in footer.
-}
module Program
	( Constant(..), constant, Word64
	, BinOp(..), binop, UnOp(..), unop
	, Id, Binds
	, ExprF(..), Fix(..), cata, Expr
	, size, Op(..), ops, free, showBS, compile, eval
	, Program(..), pSize, pOps, pFree, pShowBS, pCompile
	)
where

import Data.Word (Word64)
import Data.Bits ( (.&.), (.|.), xor, complement, shiftL, shiftR)

import           Data.ByteString             (ByteString)
import qualified Data.ByteString       as BS (intercalate)
import qualified Data.ByteString.Char8 as CS (pack, unpack)

import Data.Monoid (Monoid, (<>), mconcat)

import           Data.Map      (Map)
import qualified Data.Map as M (lookup, singleton, insert)

import           Data.Set      (Set)
import qualified Data.Set as S (empty, singleton, union, delete)

import Data.Maybe (fromMaybe)

data Constant = Zero | One
	deriving (Eq, Ord, Enum, Show)

constant :: Constant -> Word64
{-# INLINE constant #-}
constant Zero = 0
constant One  = 1

data BinOp = And | Or | Xor | Plus
	deriving (Eq, Ord, Enum, Show)

binop :: BinOp -> Word64 -> Word64 -> Word64
{-# INLINE binop #-}
binop And  = (.&.)
binop Or   = (.|.)
binop Xor  = xor
binop Plus = (+)

data UnOp = Not | Shl1 | Shr1 | Shr4 | Shr16
	deriving (Eq, Ord, Enum, Show)

unop :: UnOp -> Word64 -> Word64
{-# INLINE unop #-}
unop Not   = complement
unop Shl1  = flip shiftL 1
unop Shr1  = flip shiftR 1
unop Shr4  = flip shiftR 4
unop Shr16 = flip shiftR 15

type Id = ByteString

data ExprF a = ExprConst Constant | ExprVar !Id | ExprIf0 a a a | ExprUn !UnOp a | ExprBin !BinOp a a | ExprFold a a !Id !Id a
	deriving (Eq, Show)

instance Functor ExprF where
	fmap _ (ExprConst        c) = ExprConst c
	fmap _ (ExprVar          v) = ExprVar v
	fmap f (ExprIf0      i t e) = ExprIf0 (f i) (f t) (f e)
	fmap f (ExprUn      op e  ) = ExprUn op (f e)
	fmap f (ExprBin     op l r) = ExprBin op (f l) (f r) 
	fmap f (ExprFold l i b a e) = ExprFold (f l) (f i) b a (f e)

type Binds = [(Id, Word64)]

algCompile :: ExprF (Binds -> Word64) -> Binds -> Word64
algCompile (ExprConst        c) _     = constant c
algCompile (ExprVar          v) binds = fromMaybe (error $ "Undefined variable: " ++ CS.unpack v) $ lookup v binds
algCompile (ExprIf0      i t e) binds | i binds == 0 = t binds
                                      | otherwise    = e binds
algCompile (ExprUn      op e  ) binds = unop op $ e binds
algCompile (ExprBin     op e f) binds = binop op (e binds) (f binds)
algCompile (ExprFold e f v w g) binds = foldr (\b a -> g $ (v, b) : (w, a) : binds) i $ bytes l
 where
	l = e binds
	i = f binds

bytes :: Word64 -> [Word64]
{-# INLINE bytes #-}
bytes n = reverse $ map ((.&. 0xFF) . shiftR n . flip shiftL 3) [0..7]

algSize :: ExprF Int -> Int
algSize (ExprConst        _) = 1
algSize (ExprVar          _) = 1
algSize (ExprUn         _ e) = 1 + e
algSize (ExprBin      _ e f) = sum [1, e, f]
algSize (ExprIf0      i t e) = sum [1, i, t, e]
algSize (ExprFold l i b a e) = sum [2, l, i, e]

data Op = Un UnOp | Bin BinOp | If0 | TFold | Fold
	deriving (Eq, Ord, Show)

algOps :: ExprF (Set Op) -> Set Op
algOps (ExprConst        _) = S.empty
algOps (ExprVar          _) = S.empty
algOps (ExprUn        op e) = S.singleton (Un op)  <> e
algOps (ExprBin     op e f) = mconcat [S.singleton (Bin op), e, f]
algOps (ExprIf0      i t e) = mconcat [S.singleton If0, i, t, e]
algOps (ExprFold l i b a e) = mconcat [S.singleton Fold, l, i, e]

algFree :: ExprF (Set Id) -> Set Id
algFree (ExprConst        c) = S.empty
algFree (ExprVar          v) = S.singleton v
algFree (ExprUn      op   e) = e
algFree (ExprBin     op l r) = l <> r
algFree (ExprIf0      i t e) = mconcat [i, t, e]
algFree (ExprFold l i b a e) = mconcat [l, i, S.delete a $ S.delete b e]

extract :: ExprF e -> [e]
extract (ExprConst        _) = []
extract (ExprVar          _) = []
extract (ExprUn         _ e) = [e]
extract (ExprBin      _ l r) = [l, r]
extract (ExprIf0      i t e) = [i, t, e]
extract (ExprFold l i _ _ e) = [l, i, e]

algMconcat :: Monoid m => ExprF m -> m
algMconcat = mconcat . extract

space :: ByteString
space = CS.pack " "

spaced :: [ByteString] -> ByteString
{-# INLINE spaced #-}
spaced = BS.intercalate space

close_paren :: ByteString
close_paren = CS.pack ")"

closed :: ByteString -> ByteString
{-# INLINE closed #-}
closed = (<> close_paren)

lambda_intro :: ByteString
lambda_intro = CS.pack "(lambda ("

withLambda :: ByteString -> ByteString
withLambda = (lambda_intro <>)

algShowBS :: ExprF ByteString -> ByteString
algShowBS (ExprConst     Zero) = CS.pack "0"
algShowBS (ExprConst      One) = CS.pack "1"
algShowBS (ExprVar          v) = v
algShowBS (ExprUn     Not   e) = closed $ spaced [ CS.pack "(not",   e ]
algShowBS (ExprUn     Shl1  e) = closed $ spaced [ CS.pack "(shl1",  e ]
algShowBS (ExprUn     Shr1  e) = closed $ spaced [ CS.pack "(shr1",  e ]
algShowBS (ExprUn     Shr4  e) = closed $ spaced [ CS.pack "(shr4",  e ]
algShowBS (ExprUn     Shr16 e) = closed $ spaced [ CS.pack "(shr16", e ]
algShowBS (ExprBin   And  e f) = closed $ spaced [ CS.pack "(and",   e, f ]
algShowBS (ExprBin   Or   e f) = closed $ spaced [ CS.pack "(or",    e, f ]
algShowBS (ExprBin   Xor  e f) = closed $ spaced [ CS.pack "(xor",   e, f ]
algShowBS (ExprBin   Plus e f) = closed $ spaced [ CS.pack "(plus",  e, f ]
algShowBS (ExprIf0      i t e) = closed $ spaced [ CS.pack "(if0",   i, t, e ]
algShowBS (ExprFold l i b a e) = closed . closed $ spaced [ CS.pack "(fold", l, i, withLambda b, closed a, e ]

newtype Fix f = Fix { unFix :: f (Fix f) }

type Expr = Fix ExprF

cata :: (ExprF a -> a) -> Expr -> a
{-# INLINE cata #-}
cata alg = let c = alg . fmap c . unFix in c

compile :: Expr -> Binds -> Word64
compile = cata algCompile

eval :: Binds -> Expr -> Word64
eval = flip compile

size :: Expr -> Int
size = cata algSize

ops :: Expr -> Set Op
ops = cata algOps

free :: Expr -> Set Id
free = cata algFree

showBS :: Expr -> ByteString
showBS = cata algShowBS

data Program = Program Id Expr

pCompile :: Program -> Word64 -> Word64
pCompile (Program v e) = compile e . (: []) . ((,) v)

pSize :: Program -> Int
pSize (Program _ e)= 1 + size e

pOps :: Program -> Set Op
pOps (Program _ (Fix (ExprFold e f _ _ g))) = mconcat [ S.singleton TFold, ops e, ops f, ops g ]
pOps (Program _                        e  ) = ops e

pFree :: Program -> Set Id
pFree (Program v e) = S.delete v $ free e

pShowBS :: Program -> ByteString
pShowBS (Program v e) = closed $ spaced [ closed $ withLambda v, showBS e]

instance Show Program where
	show = CS.unpack . pShowBS

{-
This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program, in the file named "LICENSE".  If not, see
<http://www.gnu.org/licenses/>.
-}
