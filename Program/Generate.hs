{-# LANGUAGE RankNTypes #-}
{-
Copyright 2013 Boyd Stephen Smith Jr.
Copyright 2013 Zac Slade

Licensing information in footer.
-}
module Program.Generate
	( Word64, Map, Op, Id, ProgramConstraints(..), programs
	, ByteString, ValueConstraints, PreparedProgram(..), prepProgram, valueFilter
	, Refine(..), mbToRefine, refineToMb, Fix(..), RefineF(..)
	, unfilteredList, progressivelyFiltered, refinedPrograms
	)
where

import Program hiding (size)

import Util.Enum (enum)

import Control.Applicative ( (<$>), (<*>) )
import Control.Monad ( guard, (>=>), MonadPlus(..), msum )

import Data.ByteString (ByteString)
import qualified Data.ByteString.Char8 as CS (pack, unpack)
import qualified Data.Edison.Seq.SizedSeq as SS
import Data.List
import           Data.Map      (Map)
import qualified Data.Map as M (toList)
import Data.Monoid ( Monoid, (<>), mconcat )
import Data.Ord (comparing)

type ValueConstraints = Map Word64 Word64

data ProgramConstraints = ProgramConstraints
	{ maxSize :: {-# UNPACK #-} !Int
	, operators :: [Op]
	}

data ExprConstraints = ExprConstraints
	{ exactSize :: {-# UNPACK #-} !Int
	, allowedOps :: [Op]
	, neededOps :: SS.Sized [] Op
	, boundVars :: [Id]
	}

var_x :: Id
var_x = CS.pack "x"

var_y :: Id
var_y = CS.pack "y"

var_z :: Id
var_z = CS.pack "z"

data PreparedProgram = PreparedProgram
	{ bytes :: ByteString
	, function :: Word64 -> Word64
	}

instance Show PreparedProgram where
	show = CS.unpack . bytes

prepProgram :: Program -> PreparedProgram
prepProgram p = PreparedProgram
	{ bytes = pShowBS p
	, function = pCompile p
	}

programs :: ProgramConstraints -> [Program]
programs pc = do
	let
		o = operators pc
		(lanaConstr, ops) = if elem TFold o
			then (expressionsTF,  delete TFold . delete Fold $ o)
			else (expressionsNTF, o)
	ns <- [2..(maxSize pc - 1)]
	na <- reverse . subsequences $ ops
	e  <- lanaConstr ExprConstraints{ exactSize = ns, allowedOps = na, neededOps = SS.fromList na, boundVars = [var_x] }
	return $ Program var_x e

ana :: Functor f => (b -> f b) -> b -> Fix f
ana alg = let a = Fix . fmap a . alg in a

lana :: Functor f => (b -> [f b]) -> (forall a. f [a] -> [f a]) -> b -> [Fix f]
lana gen dist = let as = concatMap (map Fix . dist . fmap as) . gen  in as

lanae :: (b -> [ExprF b]) -> (forall a. ExprF [a] -> [ExprF a]) -> b -> [Expr]
{-# INLINE lanae #-}
lanae gen dist = let as = concatMap (map Fix . dist . fmap as) . gen  in as

gana :: (Functor m, Monad m, Functor f) => (b -> m (f b)) -> (forall a. f (m a) -> m (f a)) -> b -> m (Fix f)
gana gen dist = let as = gen >=> (fmap Fix . dist . fmap as) in as

distExpr :: ExprF [a] -> [ExprF a]
distExpr (ExprConst        c) = [ExprConst  c]
distExpr (ExprVar          v) = [ExprVar    v]
distExpr (ExprUn        op e) = map (ExprUn op) e

distExpr (ExprBin     op l r) = do
	left  <- l
	map (ExprBin op left ) r

distExpr (ExprIf0      i t e) = do
	if_expr   <- i
	then_expr <- t
	map (ExprIf0 if_expr then_expr) e

distExpr (ExprFold l i b a e) = do
	list <- l
	init <- i
	map (ExprFold list init b a) e

expressions, expressionsTF, expressionsNTF :: ExprConstraints -> [Expr]
expressionsNTF = genExprNTF >=> (map Fix . distExpr . fmap expressions)
expressionsTF  = foldExpr   >=> (map Fix . distExpr . fmap expressions)
expressions    = lanae genExpr distExpr

genExpr, foldExpr, genExprNTF :: ExprConstraints -> [ExprF ExprConstraints]
genExpr constr | exactSize constr <= SS.size (neededOps constr) = []
genExpr constr | exactSize constr == 1 =
	(ExprConst <$> enum) <> (ExprVar <$> boundVars constr)

genExpr constr = genExprNTF constr <> tryFoldExpr
 where
	tryFoldExpr = do
		let n = neededOps constr
		guard $ SS.fold (\e b -> b || e == Fold) False n
		let na = delete Fold $ allowedOps constr
		    nn = SS.filter (/= Fold) n
		foldExpr constr{ allowedOps = na, neededOps = nn }

subSizeSeq :: SS.Sized [] Op -> [SS.Sized [] Op]
subSizeSeq ss = case SS.lview ss of
	Nothing -> []
	Just (h, t) -> let subs = subSizeSeq t in subs ++ map (SS.lcons h) subs

{-- Does not check for Fold / TFold, only call after checking and removing
 - those from the operation sets.
 --}
foldExpr constr = do
	let s  = exactSize constr
	    nn = neededOps constr
	fs <- [1..(s-3)]
	let es = s - fs - 2
	fn <- subSizeSeq nn
	let en = SS.filter (\e -> SS.fold (\f b -> b || e == f) False fn) nn
	is <- [1..(fs-1)]
	let ls = fs - is
	ln <- subSizeSeq fn
	let i_n = SS.filter (\e -> SS.fold (\f b -> b || e == f) False ln) fn
	    l = constr{ exactSize = ls, neededOps = ln }
	    i = constr{ exactSize = is, neededOps = i_n }
	    e = constr{ exactSize = es, neededOps = en, boundVars = var_y : var_z : boundVars constr }
	return $ ExprFold l i var_y var_z e

genExprNTF constr = mconcat [ unExpr, binExpr, ifExpr ]
 where
	s = exactSize constr
	a = allowedOps constr
	n = neededOps constr
	unExpr = do
		op@(Un op_raw) <- a
		let inner = constr{ exactSize = s - 1, neededOps = SS.filter (/= op) n }
		return $ ExprUn op_raw inner
	binExpr = do
		op@(Bin op_raw) <- a
		let nn = SS.filter (/= op) n
		ls <- [1..(s-2)]
		let rs = s - ls - 1
		ln <- subSizeSeq nn
		let rn    = SS.filter (\e -> SS.fold (\f b -> b || e == f) False ln) nn
		    left  = constr{ exactSize = ls, neededOps = ln }
		    right = constr{ exactSize = rs, neededOps = rn }
		return $ ExprBin op_raw left right
	ifExpr = do
		guard $ elem If0 a
		let nn = SS.filter (/= If0) n
		bs <- [1..(s-2)]
		let is = s - bs - 1
		bn <- subSizeSeq nn
		let i_n = SS.filter (\e -> SS.fold (\f b -> b || e == f) False bn) nn
		ts <- [1..(bs-1)]
		let es = bs - ts
		tn <- subSizeSeq bn
		let en = SS.filter (\e -> SS.fold (\f b -> b || e == f) False tn) bn
		    i = constr{ exactSize = is, neededOps = i_n }
		    t = constr{ exactSize = ts, neededOps = tn  }
		    e = constr{ exactSize = es, neededOps = en  }
		return $ ExprIf0 i t e

valueFilter :: ValueConstraints -> [PreparedProgram] -> [PreparedProgram]
valueFilter values = filter (\pp -> let f = function pp in all (\(i, o) -> f i == o) $ M.toList values)

newtype RefineF c r a = RefineF (Maybe (r, c -> a))

instance Functor (RefineF c r) where
	fmap _ (RefineF Nothing)       = RefineF Nothing
	fmap f (RefineF (Just (r, n))) = RefineF $ Just (r, f . n)

newtype Refine c r = Refine { unRefine :: Fix (RefineF c r) }

mbToRefine :: Maybe (r, c -> Fix (RefineF c r)) -> Refine c r
mbToRefine = Refine . Fix . RefineF

refineToMb :: Refine c r -> Maybe (r, c -> Fix (RefineF c r))
refineToMb (Refine (Fix (RefineF mb))) = mb

instance Functor (Refine c) where
	fmap f r = case refineToMb r of
		Nothing     -> mbToRefine Nothing
		Just (r, n) -> mbToRefine $ Just (f r, unRefine . fmap f . Refine . n)

instance Monad (Refine c) where
	return v = mbToRefine $ Just (v, const . Fix $ RefineF Nothing)
	fail msg = mbToRefine Nothing
	ra >>= fb = case refineToMb ra of
		Nothing      -> mbToRefine Nothing
		Just (a, na) -> case refineToMb $ fb a of
			Nothing      -> mbToRefine Nothing
			Just (b, nb) -> mbToRefine $ Just (b, unRefine . ((Refine . na) >=> fb))

instance Monoid c => MonadPlus (Refine c) where
	mzero = mbToRefine Nothing
	mplus rl rr = case (refineToMb rl, refineToMb rr) of
		(Nothing, r) -> mbToRefine r
		(l, Nothing) -> mbToRefine l
		(Just (lv, ln), Just (rv, rn)) ->
			mbToRefine $ Just (lv, \c -> Fix . RefineF $ Just (rv, \d ->
				let cd = c <> d in unRefine $ mplus (Refine $ ln cd) (Refine $ rn cd)))

unfilteredList :: Monoid c => [r] -> Refine c r
unfilteredList = msum . map return

progressivelyFiltered :: (c -> r -> Bool) -> [r] -> Refine c r
progressivelyFiltered f = pf
 where
	pf []    = fail "Empty list"
	pf (h:t) = mbToRefine $ Just (h, \c -> unRefine . pf $ filter (f c) t)

refinedPrograms :: ProgramConstraints -> Refine ValueConstraints PreparedProgram
refinedPrograms pc = progressivelyFiltered vcFilter . map prepProgram $ programs pc
 where
	vcFilter vc pp = let f = function pp in all (\(i, o) -> f i == o) $ M.toList vc

{-
This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program, in the file named "LICENSE".  If not, see
<http://www.gnu.org/licenses/>.
-}
