{-# LANGUAGE OverloadedStrings #-}
{-
Copyright 2013 Boyd Stephen Smith Jr.
Copyright 2013 Zac Slade

Licensing information in footer.
-}
module Program.Actor
	( ActorPool, newPool
	, Actor, newActor, newPoolActor, ProgramConstraints(..)
	, sendActor, pollActor, waitActor, actorDead, killActor
	, foreverK, parseOperators
	, MessageToActor(..), MessageFromActor(..)
	, Op
	)
where

import Program
import Program.Generate

import Control.Concurrent (forkFinally, ThreadId, killThread)
import Control.Concurrent.Chan
import Control.Concurrent.MVar
import Control.Concurrent.Timeout
import Control.DeepSeq (NFData, rnf, ($!!) )
import Control.Exception (finally)

import Data.Bits (bit, complement)
import Data.ByteString (ByteString)
import Data.List (genericLength)
import System.Random (randoms, getStdGen)
import Data.IORef
import qualified Data.Map as M (singleton, fromList)
import           Data.Maybe (mapMaybe)

import System.IO (hPutStrLn, stderr)

{-- Bounded pool of workers --}
newtype ActorPool = ActorPool { slots :: Chan () }

{-- Asynchronous worker --}
data Actor = Actor
	{ recv :: MVar MessageToActor
	, send :: MVar MessageFromActor
	, dead :: IORef Bool
	, thread :: {-# UNPACK #-} !ThreadId
	}

{-- Messages to worker, must match message from worker or it will crash. --}
data MessageToActor = EvalResponse { outputs :: ![Word64] }
                    | Win | Mismatch { input :: !Word64, expectedOutput :: !Word64 }
                    | Timeout
                    | Echo !MessageFromActor

instance NFData MessageToActor where
	rnf (EvalResponse outputs)  = rnf outputs
	rnf Win                     = ()
	rnf (Mismatch input output) = rnf (input, output)
	rnf Timeout                 = ()
	rnf (Echo              msg) = rnf msg

{-- Messages from worker --}
data MessageFromActor = Eval { arguments :: ![Word64] }
                      | Guess { program :: !ByteString }
                      | ActorAlreadyDead

instance NFData MessageFromActor where
	rnf (Eval   arguments) = rnf arguments
	rnf (Guess    program) = rnf program
	rnf (ActorAlreadyDead) = ()

{-- Convenience, if another string type is better, for I/O side, please update. --}
parseOperators :: [ByteString] -> [Op]
{-# INLINE parseOperators #-}
parseOperators = mapMaybe parseOp

parseOp :: ByteString -> Maybe Op
{-# INLINE parseOp #-}
parseOp "not"   = Just $ Un Not
parseOp "shl1"  = Just $ Un Shl1
parseOp "shr1"  = Just $ Un Shr1
parseOp "shr4"  = Just $ Un Shr4
parseOp "shr16" = Just $ Un Shr16
parseOp "and"   = Just $ Bin And
parseOp "or"    = Just $ Bin Or
parseOp "xor"   = Just $ Bin Xor
parseOp "plus"  = Just $ Bin Plus
parseOp "if0"   = Just If0
parseOp "tfold" = Just TFold
parseOp "fold"  = Just Fold
parseOp     _   = Nothing

-- 5 minutes in microseconds
fiveminutes::Integer
fiveminutes = 300000000

{-- Create a new bounded, pool --}
newPool :: Int -> IO ActorPool
newPool n = do
	newSlots <- newChan
	writeList2Chan newSlots $ replicate n ()
	return ActorPool{ slots = newSlots }

{-- Create new worker that will try to solve the given constraints. --}
newActor :: ProgramConstraints -> IO Actor
{-# INLINE newActor #-}
newActor constraints = internal_newActor constraints (return ()) (return ())

{-- Create new worker in the pool that solve the given constraints. --}
newPoolActor :: ActorPool -> ProgramConstraints -> IO Actor
newPoolActor pool constraints =
	internal_newActor
		constraints
		(readChan $ slots pool)
		(writeChan (slots pool) ())

{--
 - Create a new worker, with before/after actions, that solve the given
 - constraints.
 --}
internal_newActor :: ProgramConstraints -> IO () -> IO () -> IO Actor
internal_newActor constraints before after = do
	recv <- newEmptyMVar
	send <- newEmptyMVar
	dead <- newIORef False
	thread <- forkFinally (before >> actorLoop constraints send recv) $ \_ -> do
		after
		atomicWriteIORef dead True
		tryPutMVar' send ActorAlreadyDead
		return ()
	return $ Actor recv send dead thread

{-- Strict tryPutMVar --}
tryPutMVar' :: NFData a => MVar a -> a -> IO Bool
{-# SPECIALIZE INLINE tryPutMVar' :: MVar MessageFromActor -> MessageFromActor -> IO Bool #-}
{-# SPECIALIZE INLINE tryPutMVar' :: MVar MessageToActor   -> MessageToActor   -> IO Bool #-}
tryPutMVar' mv v = tryPutMVar mv $!! v

getEvals :: IO [Word64]
getEvals = do
	rndGen <- getStdGen
	return $ initialEvals ++ randoms rndGen

{-- 256 initial evaluation requests in a neat pattern --}
initialEvals :: [Word64]
initialEvals = do
	critBit <- [0..63] 
	let single = bit critBit
	    many = single - 1
	pattern <- [single, many]
	[pattern, complement pattern]

{-- Main worker loop --}
actorLoop :: ProgramConstraints -> MVar MessageFromActor -> MVar MessageToActor -> IO ()
actorLoop constraints send recv = do
	theinputs <- getEvals
	putMVar' send $ Eval $ take 256 theinputs
	putStrLn "Actor put Eval message"
	let searchSpace = map prepProgram $ programs constraints
	foreverK loop (theinputs, searchSpace)
 where
	loop (inputs, rest) = do
		msg <- takeMVar recv
		dontdie <- timeout fiveminutes $ do
			case msg of
				Timeout -> return $ Left "TIMEOUT - Ending agent"
				Win     -> return $ Left "WIN WIN - Ending agent"
				Mismatch input output -> do
					putStrLn "Wrong, again. :("
					eval inputs (valueFilter (M.singleton input output) rest)
				EvalResponse curOutputs -> do
					putStrLn "Agent got EvalResponse."
					let values     = M.fromList $ zip inputs curOutputs
					    matches    = genericLength curOutputs
					    restInputs = drop matches inputs
					guess (valueFilter values rest) restInputs
				Echo msg       -> do
					putStrLn "ECHO Echo echo ...."
					return $ Right (msg, rest, inputs)
		case dontdie of
			Nothing -> die "Cannot find a solution in 5 minutes, get more data."
			Just (Left  errMsg) -> die errMsg
			Just (Right (msgFrom, next, inp)) -> do
				putMVar send msgFrom
				return (inp, next)
	-- Local to loop
	guess []           _ = return . Left $ "No guess; over-constrained?"
	guess (guess:next) ins = do
		putStrLn "Actor will put Guess message"
		return $ Right (Guess $!! bytes guess, next, ins)
	eval []     _ = return . Left $ "Nothing to evaluate, User Error."
	eval ins next = do
		putStrLn "Actor will put Eval message"
		return $ Right (Eval $ take 256 ins, next, ins)

{-- Exit from foreverK loops :P --}
die :: String -> IO a
die msg = putStrLn msg >> error msg

{-- Strict putMVar --}
putMVar' :: NFData a => MVar a -> a -> IO ()
{-# SPECIALIZE INLINE putMVar' :: MVar MessageFromActor -> MessageFromActor -> IO () #-}
{-# SPECIALIZE INLINE putMVar' :: MVar MessageToActor   -> MessageToActor   -> IO () #-}
putMVar' mv v = putMVar mv $!! v

{-- Reimplemented from proxy source --}
foreverK :: Monad m => (a -> m a) -> a -> m b
foreverK k = let r = \a -> k a >>= r in r

{-- Non-blocking send to worker --}
sendActor :: Actor -> MessageToActor -> IO ()
sendActor actor msg = do
	isDead <- actorDead actor
	case isDead of
		True  -> return ()
		False -> do
			sent <- tryPutMVar' (recv actor) msg
			case sent of
				True  -> return ()
				False -> hPutStrLn stderr "Concurrency assumptions violated, message lost."

{-- Non-blocking read from worker --}
pollActor :: Actor -> IO (Maybe MessageFromActor)
{-# INLINE pollActor #-}
pollActor actor = do
	isDead <- actorDead actor
	case isDead of
		True  -> return Nothing
		False -> tryTakeMVar (send actor)

{-- Blocking read from worker --}
waitActor :: Actor -> IO MessageFromActor
{-# INLINE waitActor #-}
waitActor actor = do
	isDead <- actorDead actor
	case isDead of
		True  -> return ActorAlreadyDead
		False -> takeMVar (send actor)

{-- Worker is done / dead --}
actorDead :: Actor -> IO Bool
actorDead actor = readIORef $ dead actor

{-- Kill worker by ending thread. --}
killActor :: Actor -> IO ()
killActor actor = do
	isDead <- actorDead actor
	case isDead of
		True  -> return ()
		False -> do
			killThread (thread actor)
			atomicWriteIORef (dead actor) True

{-
This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program, in the file named "LICENSE".  If not, see
<http://www.gnu.org/licenses/>.
-}
