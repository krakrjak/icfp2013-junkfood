{-# LANGUAGE OverloadedStrings      #-}
{-
Copyright 2013 Boyd Stephen Smith Jr.
Copyright 2013 Zac Slade

Licensing information in footer.
-}
module Util.WebAPI where

-- Internal includes for JSON types
import Util.WebAPI.Eval
import Util.WebAPI.Guess
import Util.WebAPI.Problem
import Util.WebAPI.TrainingProblem
import Util.WebAPI.TrainingRequest

import Data.Aeson
import Control.Applicative
import Network.Http.Client
import System.IO.Streams (InputStream, toList)
import qualified System.IO.Streams as S
import qualified Data.ByteString.Lazy as LBS
import qualified Data.ByteString as B

type APIKey = B.ByteString
type URLslug = B.ByteString

-- Site URL for playing the game
mainURL::B.ByteString
mainURL = "http://icfpc2013.cloudapp.net/"

-- Every player has his own site key so we have to build the url
fullURL::APIKey -> URLslug -> URL
fullURL key slug = B.concat [mainURL, slug, "?auth=", key]

-- Utility function to parse JSON responses
parseResponse::FromJSON a => InputStream B.ByteString -> IO (Maybe a)
parseResponse i = decode . LBS.fromChunks <$> toList i

encodeBody::ToJSON a => a -> IO (InputStream B.ByteString)
encodeBody x = S.fromByteString $ B.concat . LBS.toChunks $ encode x

-- Retrieves a list of problems from the site
myproblems::APIKey -> IO (Either (Int, B.ByteString) [Problem])
myproblems key = do
	post (fullURL key "myproblems") "text/json" emptyBody responseHandler

-- Generate test data to train with from the site
train::APIKey -> Maybe TrainingRequest -> IO (Either (Int, B.ByteString) TrainingProblem)
train key (Just req) = do
	body <- encodeBody req
	post (fullURL key "train") "text/json" (inputStreamBody body) responseHandler
train key Nothing = do
	post (fullURL key "train") "text/json" emptyBody responseHandler

-- Evaluate a program in the \BV language using the site
eval::APIKey -> EvalRequest -> IO (Either (Int, B.ByteString) EvalResponse)
eval key req = do
	body <- encodeBody req
	post (fullURL key "eval") "text/json" (inputStreamBody body) responseHandler

-- Try to solve a problem!  Good Luck!
guess::APIKey -> Guess -> IO (Either (Int, B.ByteString) GuessResponse)
guess key guess = do
	body <- encodeBody guess
	post (fullURL key "guess") "text/json" (inputStreamBody body) responseHandler

responseHandler::FromJSON a => Response -> InputStream B.ByteString 
	-> IO (Either (Int, B.ByteString) a)
responseHandler res i = do
	let code = getStatusCode res
	case code of
		200 -> do
			val <- parseResponse i
			case val of
				Just a -> return $ Right a
				Nothing -> return $ Left (200, "200: failed to parse JSON.")
		400 -> return $ Left (400, "400: Bad Request, input malformed")
		401 -> return $ Left (401, "401: Problem was not requested by current user")
		404 -> return $ Left (404, "404: Not Found")
		410 -> return $ Left (410, "410: Problem requested more than 5 minutes ago")
		412 -> return $ Left (412, "412: Already solved this one")
		413 -> return $ Left (413, "413: Request too big")
		429 -> return $ Left (429, "429: Try again")

{-
This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program, in the file named "LICENSE".  If not, see
<http://www.gnu.org/licenses/>.
-}
