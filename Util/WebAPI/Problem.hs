{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric     #-}
{-
Copyright 2013 Boyd Stephen Smith Jr.
Copyright 2013 Zac Slade

Licensing information in footer.
-}
module Util.WebAPI.Problem where

import GHC.Generics
import Control.Applicative
import Control.Monad
import Data.Aeson
import qualified Data.ByteString as B

type JString = B.ByteString

data Problem = Problem
	{ id :: JString
	, size :: Int
	, operators :: [JString]
	, solved :: Maybe Bool
	, timeLeft :: Maybe Int
	} deriving (Show,Generic)
instance ToJSON Problem

instance FromJSON Problem where
	parseJSON (Object v) =
		Problem <$> v .: "id"
		        <*> v .: "size"
			<*> v .: "operators"
			<*> v .:? "solved"
			<*> v .:? "timeLeft"
	parseJSON _ = mzero

{-
This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program, in the file named "LICENSE".  If not, see
<http://www.gnu.org/licenses/>.
-}
