{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric     #-}
{-
Copyright 2013 Boyd Stephen Smith Jr.
Copyright 2013 Zac Slade

Licensing information in footer.
-}
module Util.WebAPI.Eval where

import GHC.Generics
import Control.Applicative
import Control.Monad
import Data.Aeson
import qualified Data.ByteString as B

type JString = B.ByteString

data EvalRequest = EvalRequest
	{ -- Only provide id or program, not both
	  id :: Maybe JString
	, program :: Maybe JString
	, arguments :: [JString]
	} deriving (Show,Generic)
instance ToJSON EvalRequest

instance FromJSON EvalRequest where
	parseJSON (Object v) =
		EvalRequest <$> v .:? "id"
		            <*> v .:? "program"
			    <*> v .:  "arguments"
	parseJSON _ = mzero

data EvalResponse = EvalResponse
	{ status :: JString -- Can be "ok" or "error"
	, outputs :: Maybe [JString] -- Not present if status is error
	, message :: Maybe JString -- Not present if status is ok
	} deriving (Show,Generic)
instance ToJSON EvalResponse

instance FromJSON EvalResponse where
	parseJSON (Object v) =
		EvalResponse <$> v .:  "status"
		             <*> v .:? "outputs"
			     <*> v .:? "message"
	parseJSON _ = mzero

{-
This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program, in the file named "LICENSE".  If not, see
<http://www.gnu.org/licenses/>.
-}
