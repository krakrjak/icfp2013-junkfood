# Who
    Boyd Stephen Smith Jr <boyd.stephen.smith.jr@gmail.com>
    Zac Slade <krakrjak@gmail.com>
# What 
    http://research.microsoft.com/en-us/events/icfpcontest2013/
# When
    2013-08-09 00:00 UTC - 2013-08-12 00:00 UTC
# Where
    Fayetteville, Arkansas USA
# Why
    Deeper understanding of functional programming techniques to solve problems.

This work is licensed under the GNU Public License version 3 or later.
See LICENSE file for details.
