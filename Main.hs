{-
Copyright 2013 Boyd Stephen Smith Jr.
Copyright 2013 Zac Slade

Licensing information in footer.
-}
module Main (main) where

import           Program.Actor          ( foreverK, killActor, newActor, sendActor, parseOperators, Actor, pollActor
                                        , MessageFromActor(ActorAlreadyDead), ProgramConstraints(..) 
					, MessageToActor(..), newPool, newPoolActor, ActorPool )
import qualified Program.Actor as Actor ( MessageFromActor(Guess, Eval), program, arguments )

import Util.WebAPI
import qualified Util.WebAPI.Guess as UWG (Guess(..), GuessResponse(..))
import qualified Util.WebAPI.Problem as UWP
import qualified Util.WebAPI.Eval as UWE

import Prelude hiding (id, readFile)

import           Data.ByteString (ByteString, readFile)
import qualified Data.ByteString.Char8 as CS
import Control.Applicative
import Control.Concurrent (threadDelay, getNumCapabilities, yield)
import Data.Maybe (fromMaybe)
import           Data.Map      (Map, elems)
import qualified Data.Map as M 
import Data.Monoid ((<>))
import Data.List (sortBy, genericLength)
import Data.Function (on)
import Data.Time.Clock (UTCTime, NominalDiffTime, getCurrentTime)
import Numeric (showHex)

data AppState = AppState
	{ apiKey :: !APIKey
	, windowStart :: {-# UNPACK #-} !UTCTime
	, problemData :: Map ByteString ProblemData
	}

data ProblemData = ProblemData
	{ problemSpace :: !Integer
	, timeLeft :: Maybe Int
	, actor :: !Actor
	}

mkProblemData :: ActorPool -> UWP.Problem -> IO (ByteString, ProblemData)
mkProblemData pool p = do
	actor <- newPoolActor pool ProgramConstraints{ maxSize = UWP.size p, operators = parseOperators $ UWP.operators p }
	let
		pd = ProblemData
			{ problemSpace = genericLength (UWP.operators p) ^ UWP.size p
			, timeLeft = UWP.timeLeft p
			, actor = actor
			}
	return (UWP.id p, pd)

us_per_request :: Int
us_per_request = 4 * 1000 * 1000

main :: IO ()
main = do
	contents <- readFile "apikey"
	case take 1 $ CS.lines contents of
	    [] -> putStrLn "No contents in the apikey file."
	    [key] -> do
		putStrLn "Read APIKey"
		startWindow <- getCurrentTime
		problems <- myproblems key
		case problems of
			Left (429, _) -> do
				putStrLn "[Problems] We need to back off...  submitting too fast." 
				threadDelay us_per_request -- No need to update state, we are just getting started.
				main
			Left (_  , errmsg) -> print errmsg
			Right (todo) -> do
				putStrLn "Got a set of problems, diving in!"
				procs <- getNumCapabilities
				let poolSize = max 2 $ procs
				putStrLn $ ("Using pool with " ++) . shows poolSize $ " slots."
				pool <- newPool poolSize
				pdl <- mapM (mkProblemData pool) $ sortBy problemSort [ p | p <- todo, not . fromMaybe False $ UWP.solved p, (0 /=) . fromMaybe 300 $ UWP.timeLeft p ] 
				foreverK mainLoop AppState{ apiKey = key, windowStart = startWindow, problemData = M.fromList pdl }

mainLoop state = do
	m_trip <- findAgentMessage $ sortBy (((nothingLasts `on` timeLeft) <> pdataSort) `on` snd) . M.toList $ problemData state 
	case m_trip of
		Nothing -> if M.null $ problemData state then return $ error "Done." else yield >> return state
		Just (id, actor, msg) -> do
			let deleteId = return state{ problemData = M.delete id $ problemData state}
			    timeoutId = sendActor actor Timeout >> deleteId
			    killId = killActor actor >> deleteId
			case msg of
				ActorAlreadyDead -> putStrLn "Got a message from a dead Actor." >> deleteId
				Actor.Guess{}    -> do
					putStrLn "Sent a guess, processing results."
					let req = UWG.Guess { UWG.id = id, UWG.program = Actor.program msg }
					m_resp <- guess (apiKey state) req
					case m_resp of
						Left (429, m) -> do
						    sendActor actor $ Echo msg
						    print m >> retry state
						Left (410  , m) -> print m >> timeoutId
						Left (412,  m) -> print m >> killId
						Left (_  , m) -> print m >> killId
						Right (resp)  -> case UWG.status resp of
							win      | CS.pack "win"      == win      -> do
								putStrLn "We solved one!"
								sendActor actor Win >> deleteId
							mismatch | CS.pack "mismatch" == mismatch -> do
								putStrLn "We got a mismatch, requeue."
								vals <- return $ UWG.values resp
								case vals of
									Just a -> do
										sendActor actor Mismatch{ input = (map (read . CS.unpack)  a) !! 0, expectedOutput = (map (read . CS.unpack)  a) !! 1 }
										return state
									Nothing -> do
										putStrLn "Invalid JSON response from server during guess!" >> killId
							v -> do
								putStrLn $ CS.unpack v
								print (CS.unpack <$> UWG.message resp)
								killId
				Actor.Eval{} -> do
					putStr "Performing an Eval. Complexity: "
					print . problemSpace $ M.findWithDefault (ProblemData{ actor = actor, problemSpace = 0, timeLeft = Nothing }) id (problemData state)
					let req = UWE.EvalRequest { UWE.id = Just id, UWE.program = Nothing, UWE.arguments = map (CS.pack . ("0x" ++) . ($ "") . showHex) $ Actor.arguments msg }
					m_resp <- eval (apiKey state) req
					case m_resp of
						Left (429, m) -> do
						    sendActor actor $ Echo msg
						    print m >> retry state
						Left (410,  m) -> print m >> timeoutId
						Left (412,  m) -> print m >> killId
						Left (_ ,  m) -> print m >> killId
						Right  (resp) -> case UWE.status resp of
							ok | CS.pack "ok" == ok -> do
								putStrLn "Eval status OK."
								vals <- return $ UWE.outputs resp
								case vals of
								    Just outs -> sendActor actor EvalResponse{ outputs = map (read . CS.unpack) outs } >> return state
								    Nothing -> killId
							v -> do
								putStrLn $ CS.unpack v
								print (CS.unpack <$> UWE.message resp)
								killId

nothingLasts::Ord a => Maybe a -> Maybe a -> Ordering
nothingLasts Nothing Nothing = EQ
nothingLasts _       Nothing = LT
nothingLasts Nothing _       = GT
nothingLasts l       r       = l `compare` r

pdataSort::ProblemData -> ProblemData -> Ordering
pdataSort l r = problemSpace l `compare` problemSpace r

problemSort::UWP.Problem -> UWP.Problem -> Ordering
problemSort l r = compare (genericLength (UWP.operators l) ^ UWP.size l) (genericLength (UWP.operators r) ^ UWP.size r)

findAgentMessage::[(ByteString, ProblemData)] -> IO (Maybe (ByteString, Actor, MessageFromActor))
findAgentMessage [] = return Nothing
findAgentMessage (h:t) = do
	m_msg <- pollActor . actor $ snd h
	case m_msg of
		Nothing -> findAgentMessage t
		Just m -> return $ Just (fst h, actor $ snd h, m)

retry::AppState->IO AppState
retry state = do
	putStrLn "[Guess] We need to back off...  submitting too fast." 
	threadDelay us_per_request
	now <- getCurrentTime
	return $ state{ windowStart = now }

{-
This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program, in the file named "LICENSE".  If not, see
<http://www.gnu.org/licenses/>.
-}
